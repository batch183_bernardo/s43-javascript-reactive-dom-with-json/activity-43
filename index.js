// console.log("Hello World");
// Scenario: Let's create a simple program that will perform CRUD operator for posting the available movies to watch.

// Mock Database
let posts = [];
// Post ID
let count = 1;

//Add post data.
//This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
		
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	//Prevent the page from loading
	e.preventDefault();

	posts.push({
		// "id" property will be used for the unique identification of each post
		id: count,
		// "title" & "body" values will come fromt the "form-add-post" input elemets.
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// count will increment every time a new movie is added
	count++;

	console.log(posts);
	alert("Successfully Added!");
	showPost();
})

// View Posts
const showPost = () =>{
	let postEntries = "";

	// We will used forEach() to display each movie inside our mock database.
	posts.forEach((post) =>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Post Button
// We will create a function that will be called in the onclick() event and will pass the value in the update form input box.

const editPost = (id) =>{
	// Contain the value of the title and body in a variabloe
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body of the movie post to be updated in the Edit Post/Form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

}

// Update Post
// This will trigger an event that will update a post upon clicking the Update button.

document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	let postId = document.querySelector("#txt-edit-id").value
	let title = document.querySelector("#txt-edit-title").value
	let body = document.querySelector("#txt-edit-body").value
	// Use a loop that will check each post stored in our mock database.
	for(i = 0; i < posts.length; i++){
		//Use a if statement to look for the post to be updated using id property.
		if(posts[i].id == postId){
			// reassign the value of the title and body property.
			posts[i].title = title;
			posts[i].body = body;

			// Invoke the showPost() to check the updated posts.
			showPost();

			alert("Successfully Updated!");

			break;
		}
	}

})

// Activity Delete Post

const deletePost = (id) =>{
	document.querySelector("#div-post-entries").addEventListener("submit", (e) =>{

	e.preventDefault();

	for(i = 0; i < posts.length; i++){
		if(posts.length != 0){

			posts.splice(i, 1)

			alert("Successfully Deleted!");
			
			break;
		}
	}



})
}


